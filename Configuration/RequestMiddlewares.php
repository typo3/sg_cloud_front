<?php

use SGalinski\SgCloudFront\Middleware\ResourceReplacerMiddleware;

return [
	'frontend' => [
		'sgalinski/sg-cloud-front' => [
			'target' => ResourceReplacerMiddleware::class,
			'description' => '',
			'before' => [
				'typo3/cms-frontend/content-length-headers',
				'sgalinski/scriptmerger',
				'sgalinski/content-replacer',
				'fluidtypo3/vhs/asset-inclusion',
				"staticfilecache/prepare"
			]
		]
	]
];
