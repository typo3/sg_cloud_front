<?php

use SGalinski\SgCloudFront\Middleware\ResourceReplacerMiddleware;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();

	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgCloudFront\\', __DIR__ . '/../Classes/');

	$services->get(ResourceReplacerMiddleware::class)
		->public()
		->autowire();
};
