<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (http://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgCloudFront\Service;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use UnexpectedValueException;

/**
 * Class for the functions, which are extracting the resources from content
 */
class ResourceExtractionService {
	/**
	 * List of file types which shall be parsed from A-tag href-attributes.
	 *
	 * @var array
	 */
	protected $recognizedAHrefFileTypes = [
		// Origin: http://www.youneeditall.com/web-design-and-development/file-extensions.html
		// This list is modified.

		// Text Files Types and Formats
		'.doc', '.docx', '.log', '.msg', '.rtf', '.odt', '.pages', '.txt', '.wpd', '.wps',

		// Data Files Types and Formats
		'.csv', '.dat', '.efx', '.gbr', '.key', '.pps', '.ppt', '.pptx', '.sdf', '.tax2010', '.vcf',
		//'.xml',

		// Audio File Types and Formats
		'.aif', '.iff', '.m3u', '.m4a', '.mid', '.mp3', '.mpa', '.ra', '.wav', '.wma',

		// Video Files Types and Formats
		'.3g2', '.3gp', '.asf', '.asx', '.avi', '.flv', '.mov', '.mp4', '.mpg', '.rm', '.swf', '.vob', '.wmv',

		// 3D Image Files Types and Formats
		'.3dm', '.max',

		// Raster Image Files Types and Formats
		'.bmp', '.gif', '.jpg', '.png', '.psd', '.pspimage', '.thm', '.tif', '.yuv', '.webp',

		// Vector Image Files Types and Formats
		'.ai', '.drw', '.eps', '.ps', '.svg',

		// Page Layout Files Types and Formats
		'.indd', '.pct', '.pdf', '.qxd', '.qxp', '.rels',

		// Spreadsheet Files Types and Formats
		'.xlr', '.xls', '.xlsx',

		// Database Files Types and Formats
		'.accdb', '.db', '.dbf', '.mdb', '.pdb', '.sql',

		// Executable Files Types and Formats
		'.app', '.bat', '.cgi', '.com', '.exe', '.gadget', '.jar', '.pif', '.vb', '.wsf',

		// Game Files Types and Formats
		'.gam', '.nes', '.rom', '.sav',

		// CAD Files Types and Formats
		'.dwg', '.dxf',

		// GIS Files Types and Formats
		'.gpx', '.kml',

		// Web Files Types and Formats
//		'.asp', '.cer', '.csr', '.css', '.htm', '.html', '.js', '.jsp', '.php', '.rss', '.xhtml',

		// Plugin Files Types and Formats
		'.8bi', '.plugin', '.xll',

		// Font Files Types and Formats
		'.fnt', '.fon', '.otf', '.ttf',

		// System Files Types and Formats
		'.cab', '.cpl', '.cur', '.dll', '.dmp', '.drv', '.lnk', '.sys',

		// Settings File Types and Formats
		'.cfg', '.ini', '.keychain', '.prf',

		// Encoded Files Types and Formats
		'.bin', '.hqx', '.mim', '.uue',

		// Compressed Files Types and Formats
		'.7z', '.deb', '.gz', '.gzip', '.pkg', '.rar', '.rpm', '.sit', '.sitx', '.tar', '.tar.bz2', '.tar.gz', '.tbz2',
		'.tgz', '.zip', '.zipx',

		// Disk Image Files Types and Formats
		'.dmg', '.iso', '.toast', '.vcd',

		// Developer Files Types and Formats
		'.c', '.class', '.cpp', '.cs', '.dtd', '.fla', '.java', '.m', '.pl', '.py',

		// Backup Files Types and Formats
		'.bak', '.gho', '.ori', '.tmp',

		// Misc Files Types and Formats
		'.dbx', '.msi', '.part', '.torrent',
	];

	/**
	 * Extracts resource paths from the given rendered document.
	 *
	 * @param string $renderedDocument
	 * @return array
	 */
	public function extractResourcePaths(string $renderedDocument): array {
		// JavaScript includes
		$resourcePaths[] = $this->getIncludedJavaScriptFilePaths($renderedDocument);

		// CSS files and files referenced from them
		$resourcePaths[] = $this->getAllCSSFilePaths($renderedDocument);

		// SOURCE tags
		$resourcePaths[] = $this->getSourcePathsFromSourceTags($renderedDocument);

		// Favicon
		$resourcePaths[] = $this->getFaviconPaths($renderedDocument);

		// IMG tags
		$resourcePaths[] = $this->getSourcePathsFromImgTags($renderedDocument);

		// INPUT tags
		$resourcePaths[] = $this->getSourcePathsFromImageInputTags($renderedDocument);

		// News Feed
		$resourcePaths[] = $this->getNewsFeedPath($renderedDocument);

		// Href stuff
		$resourcePaths[] = $this->getPathsFromATags($renderedDocument);

		// SWF via JavaScript
		$resourcePaths[] = $this->getSwfObjects($renderedDocument);

		// objects (embed)
		$resourcePaths[] = $this->getEmbeddedObjectPaths($renderedDocument);

		// Apple Touch Icons
		$resourcePaths[] = $this->getAppleTouchIcons($renderedDocument);

		// JSON File Paths
		$resourcePaths[] = $this->getJsonFilePaths($renderedDocument);

		// remove empty entries
		return array_merge(...$resourcePaths);
	}

	/**
	 * Returns a list of all IMG tag's 'src' attributes that are contained
	 * in the given content.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getSourcePathsFromImgTags(string $content): array {
		return $this->getAttributeFromTag('img', 'src', $content);
	}

	/**
	 * Returns a list of all SOURCE tag's 'srcset' attributes that are contained
	 * in the given content.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getSourcePathsFromSourceTags(string $content): array {
		$paths = $this->getAttributeFromTag('source', 'srcset', $content);
		$newPaths = [];
		foreach ($paths as $path) {
			$original = trim($path['original']);
			$normalized = trim($path['normalizedPath']);

			$parts = explode(' ', $original);
			$normalizedParts = explode(' ', $normalized);
			foreach ($parts as $index => $part) {
				if (is_numeric($part[0])) {
					continue;
				}

				$newPaths[] = [
					'original' => trim($part),
					'normalizedPath' => trim($normalizedParts[$index])
				];
			}
		}
		return $newPaths;
	}

	/**
	 * Returns a list of all INPUT tag's 'src' attribute values that are contained
	 * in the given content.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getSourcePathsFromImageInputTags(string $content): array {
		return $this->getAttributeFromTag('input', 'src', $content);
	}

	/**
	 * Returns a list of all included JavaScript file paths.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getIncludedJavaScriptFilePaths(string $content): array {
		return $this->getAttributeFromTag('script', 'src', $content);
	}

	/**
	 * Returns a list of object paths which are referenced within object-tags.
	 *
	 * @param string $content
	 * @return  array
	 */
	public function getEmbeddedObjectPaths(string $content): array {
		return $this->getAttributeFromTag('embed', 'src', $content);
	}

	/**
	 * Returns a list of object paths which are referenced within object-tags.
	 *
	 * @param string $content
	 * @return  array
	 */
	public function getAppleTouchIcons(string $content): array {
		return $this->getAttributeFromTag('link', 'href', $content, ['rel' => 'apple-touch-icon']);
	}

	/**
	 * Returns a list of object paths which are referenced within object-tags.
	 *
	 * @param string $content
	 * @param string $requiredPathSuffix
	 * @return  array
	 */
	public function getNewsFeedPath(string $content, string $requiredPathSuffix = '.xml'): array {
		$paths = $this->getAttributeFromTag('link', 'href', $content, ['rel' => 'alternate']);
		$requiredPathSuffixLength = strlen($requiredPathSuffix);
		if ($requiredPathSuffixLength > 0) {
			foreach ($paths as $key => $path) {
				$expectedSuffixPosition = (strlen($path['original']) - $requiredPathSuffixLength);
				if (strpos($path['original'], $requiredPathSuffix) !== $expectedSuffixPosition) {
					unset($paths[$key]);
				}
			}
		}

		return $paths;
	}

	/**
	 * Returns a list of file paths parsed from calls to the swfObject JavaScript library.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getSwfObjects(string $content): array {
		$paths = [];
		$matches = [];
		preg_match_all(
			'~swfObject\.embedSWF\(\s?[\'"](.*?)[\'"]~is',
			$content,
			$matches,
			PREG_PATTERN_ORDER
		);

		if (is_array($matches) && count($matches) > 1 && is_array($matches[1])) {
			$paths = $matches[1];
		}

		return $this->normalizePaths(
			$this->applyFiltersToFilePaths($paths)
		);
	}

	/**
	 * Returns a list of file paths parsed from json tags.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getJsonFilePaths(string $content): array {
		$matches = [];
		$count = preg_match_all('(<script\s[^>]*>\s[^<]*</script>)', $content, $matches);
		if ($count <= 0) {
			return [];
		}

		$paths = [];
		foreach ($matches[0] as $scriptTag) {
			$type = $this->getAttributeFromTag('script', 'type', $scriptTag);
			if (!isset($type[0]['original'])) {
				continue;
			}

			$type = $type[0]['original'];
			if ($type !== 'application/ld+json' && $type !== 'application/json') {
				continue;
			}

			$scriptMatches = [];
			$count = preg_match_all('"\s*https?://[^/\"]*([^\"]*)\s*"', $scriptTag, $scriptMatches);
			if ($count <= 0) {
				continue;
			}

			foreach ($scriptMatches[1] as $url) {
				$url = trim($url);
				if (empty($url)) {
					continue;
				}

				$dotPosition = strrpos($url, '.');
				if ($dotPosition !== FALSE && in_array(
					strtolower(
						substr($url, $dotPosition)
					),
					$this->recognizedAHrefFileTypes,
					TRUE
				)
				) {
					$paths[] = $url;
				}
			}
		}

		return $this->normalizePaths(
			$this->applyFiltersToFilePaths($paths)
		);
	}

	/**
	 * Returns a list of all favicon referenced in the given content.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getFaviconPaths(string $content): array {
		return array_merge(
			$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'shortcut icon']),
			array_merge(
				$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'icon']),
				$this->getAttributeFromTag('link', 'href', $content, ['type' => 'image/vnd.microsoft.icon'])
			)
		);
	}

	/**
	 * Returns a list of all files that are referenced within A-tags and
	 * are 'downloadable.'
	 *
	 * @param string $content
	 * @return array
	 */
	public function getPathsFromATags(string $content): array {
		$paths = [];
		$allPaths = $this->getAttributeFromTag('a', 'href', $content);
		foreach ($allPaths as $key => $path) {
			$dotPosition = strrpos($path['normalizedPath'], '.');
			if ($dotPosition !== FALSE &&
				in_array(
					strtolower(substr($path['normalizedPath'], $dotPosition)),
					$this->recognizedAHrefFileTypes,
					TRUE
				)
			) {
				$paths[$key]['original'] = $path['original'];
				$paths[$key]['normalizedPath'] = $path['normalizedPath'];
			}
		}

		return $paths;
	}

	/**
	 * Returns a list of all CSS files which are referenced within the given content
	 * and all files which are referenced from the CSS itself.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getAllCSSFilePaths(string $content): array {
		// fetch css files referenced by link-tag
		$resourcePaths = $this->getIncludedCSSFiles($content);

		// Resources from CSS files
		// Why should we fetch resources from the external CSS files!? We don't touch them.
		//        $tmpPaths = [];
		//        foreach ($resourcePaths as $cssFile) {
		//            $cssPath = $cssFile['original'];
		//            if ($this->fileExists($cssPath)) {
		//                $includedCssRes = $this->extractResourcePathsFromCSS(
		//                    $this->getFileContent($cssPath)
		//                );
		//                foreach ($includedCssRes as $res) {
		//                    $tmpPaths[] = $this->normalizePath($res, $cssPath);
		//                }
		//            }
		//        }
		//        $resourcePaths = array_merge($resourcePaths, $tmpPaths);

		// Resources from embedded and inline CSS
		$embeddedCSSFiles = $this->extractResourcePathsFromCSS(
			implode(' ', $this->getStyleBlocksFromMarkup($content))
		);
		return array_merge(
			$resourcePaths,
			$this->normalizePaths($embeddedCSSFiles),
			$this->normalizePaths($this->getPathsFromInlineCSS($content))
		);
	}

	/**
	 * Finds attributes in HTML tags.
	 *
	 * @param string $tagName The name of the tag. E.g. 'img'.
	 * @param string $attributeName The attribute name.
	 * @param string $content The content to parse.
	 * @param array $requiredOtherAttributes An array of other attributes the tag must contain. This has to be an associative array where the key of an element is the attribute's name, and the element's value is the attribute's value. This param is optional.
	 * @return array
	 */
	public function getAttributeFromTag(
		string $tagName,
		string $attributeName,
		string $content,
		array $requiredOtherAttributes = []
	): array {
		$paths = [];
		$matches = [];
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$pattern = '~<';
		$pattern .= $tagName;
		$pattern .= '\b[^>]+\b';
		$pattern .= $attributeName;
		$pattern .= '\s?=\s?[\'"](.*?)[\'"][^>]*>~is';
		$count = preg_match_all($pattern, $content, $matches, PREG_PATTERN_ORDER);

		if ($count > 0 && is_array($matches[1]) && count($matches[1]) > 0) {
			if (count($requiredOtherAttributes) > 0) {
				foreach ($matches[1] as $mkey => $match) {
					$containsAllRequiredAttributes = TRUE;
					foreach ($requiredOtherAttributes as $key => $attr) {
						$attrMatches = [];
						$attrPattern = '~' . preg_quote($key, '~') . '=["\']' . preg_quote($attr, '~') . '["\']~is';
						if (preg_match_all($attrPattern, $matches[0][$mkey], $attrMatches, PREG_PATTERN_ORDER) === 0) {
							$containsAllRequiredAttributes = FALSE;
						}
					}
					if ($containsAllRequiredAttributes) {
						$paths[] = $match;
					}
				}
			} else {
				$paths = $matches[1];
			}
		}

		return $this->normalizePaths(
			$this->applyFiltersToFilePaths($paths)
		);
	}

	/**
	 * Applies the configured filters to the given paths.
	 * This includes removal of auto-generated files (those whose filename
	 * contains a '?') and to exclude filters.
	 *
	 * @param array $paths
	 * @return array The filtered path list.
	 */
	public function applyFiltersToFilePaths(array $paths): array {
		$filteredPaths = [];

		if (count($paths) > 0) {
			foreach ($paths as $path) {
				if (str_starts_with($path, '//')) {
					continue;
				}

				$slashPos = strpos($path, '/');
				if ($slashPos === 0) {
					$path = substr($path, 1);
				}

				$filteredPaths[] = $path;
			}
		} else {
			$filteredPaths = $paths;
		}

		return $filteredPaths;
	}

	/**
	 * Returns a list of all referenced stylesheets in the given markup.
	 * This includes files referenced via link tag and via the CSS-import statement.
	 *
	 * @param string $content The (X)HTML markup to parse.
	 * @return array
	 */
	public function getIncludedCSSFiles(string $content): array {
		return array_merge(
			$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'stylesheet']),
			$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'preload']),
			$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'prefetch']),
			$this->getAttributeFromTag('link', 'href', $content, ['rel' => 'preconnect']),
			$this->getAttributeFromTag('link', 'href', $content, ['type' => 'text/css'])
		);
	}

	/**
	 * Extracts paths to resources in CSS code.
	 * This means file references which are included in "url(...)".
	 *
	 * @param string $cssContent
	 * @return array
	 */
	public function extractResourcePathsFromCSS(string $cssContent): array {
		$paths = [];
		$matches = [];
		preg_match_all(
			'~url\(\s*[\'"]?(/?(\.\./)?.*?)[\'"]?;?\s*\)~is',
			$cssContent,
			$matches,
			PREG_PATTERN_ORDER
		);

		if (is_array($matches) && count($matches) > 1 && is_array($matches[1])) {
			$paths = $matches[1];
		}

		if (count($paths) > 0) {
			$allPaths = $paths;
			$paths = [];
			foreach ($allPaths as $path) {
				if (!str_contains($path, ',')) {
					$paths[] = $path;
				}
			}
		}

		return $paths;
	}

	/**
	 * Returns the inner content of all <style></style> blocks of the given markup as an array.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getStyleBlocksFromMarkup(string $content): array {
		$blocks = [];
		$matches = [];
		preg_match_all(
			'~<style[^>]*>(.*?)</style>~is',
			$content,
			$matches,
			PREG_PATTERN_ORDER
		);

		if (is_array($matches) && count($matches) > 1 && is_array($matches[1])) {
			$blocks = $matches[1];
		}

		return $blocks;
	}

	/**
	 * Returns a list of all resource paths that are referenced by url() in the
	 * document's inline CSS.
	 *
	 * @param string $content
	 * @return array
	 */
	public function getPathsFromInlineCSS(string $content): array {
		$paths = [];
		$matches = [];
		preg_match_all(
			'~style=".*?url\((.*?)\).*?"~is',
			$content,
			$matches,
			PREG_PATTERN_ORDER
		);

		if (is_array($matches) && count($matches) > 1 && is_array($matches[1])) {
			foreach ($matches[1] as $match) {
				if ($match === '\'/\'' || $match === '"/"' || $match === '/') {
					continue;
				}

				$paths[] = $match;
			}
		}

		return $paths;
	}

	/**
	 * Makes a path relative to the webroot.
	 *
	 * @param string $path The path.
	 * @param string $referenceOrigin A file path or directory from which the file defined by $path is referenced from.
	 * @return array
	 */
	public function normalizePath(string $path, string $referenceOrigin = ''): array {
		$path = trim($path);
		$paths = [];
		$paths['original'] = $path;

		// Remove " and '
		$path = str_replace(['"', '\''], '', $path);

		// Remove the caching timestamp
		$questionMarkStrPos = strpos($path, '?');
		if ($questionMarkStrPos) {
			$path = (string) substr($path, 0, $questionMarkStrPos);
		}

		if ($this->containsProtocol($path)) {
			$normalizedPath = $path;
			if ($this->urlPointsToThisSite($path)) {
				$normalizedPath = str_replace($this->getSiteUrl(), '', $path);
			}
		} elseif (str_starts_with($path, '/')) {
			$normalizedPath = substr($path, 1);
		} else {
			if (str_starts_with($path, './')) {
				$path = str_replace('./', '', $path);
			}

			// calculate path dependent on origin
			if ($referenceOrigin !== '') {
				$path = str_replace(
					$this->getEqualPartOfPaths($path, $referenceOrigin),
					'',
					$path
				);

				$numDirsUp = substr_count($path, '../');

				$origin = $this->bubbleUpstairs(
					$this->normalizePath($referenceOrigin)['original'],
					$numDirsUp
				);

				$path = str_replace('../', '', $path);

				$normalizedPath = $origin . '/' . $path;

				// can't do anything because there's no origin
			} else {
				$normalizedPath = str_replace(
					$this->getPathToWebRoot(),
					'',
					$path
				);
			}

			// remove leading slash
			if ($this->isAbsoluteFilesystemPath($normalizedPath)) {
				$normalizedPath = substr($normalizedPath, 1);
			}
		}

		// remove '..' inside the path
		if (str_contains($normalizedPath, '..')) {
			$normalizedPath = str_replace($this->getPathToWebRoot(), '', realpath($normalizedPath));
		}

		$paths['normalizedPath'] = $normalizedPath;
		return $paths;
	}

	/**
	 * Returns a part of the path of both paths which is the same in both.
	 * For example, if you pass 'fileadmin/templates/css/style.css' and
	 * 'fileadmin/templates/images/icon.png' the method will return
	 * 'fileadmin/templates/'.
	 *
	 * @param string $path1
	 * @param string $path2
	 * @return string
	 */
	public function getEqualPartOfPaths(string $path1, string $path2): string {
		$maxLength = min([strlen($path1), strlen($path2)]);
		$currentPos = 0;
		$matched = TRUE;

		while ($currentPos < $maxLength && $matched === TRUE) {
			if ($path1[$currentPos] !== $path2[$currentPos]) {
				$matched = FALSE;
			} else {
				$currentPos++;
			}
		}

		return substr($path1, 0, $currentPos);
	}

	/**
	 * Makes each given path relative to the webroot.
	 *
	 * @param array $paths The paths.
	 * @return array
	 */
	public function normalizePaths(array $paths): array {
		if (count($paths) > 0) {
			foreach ($paths as $key => $path) {
				$paths[$key] = $this->normalizePath($path);
			}
		}

		return $paths;
	}

	/**
	 * Bubbles up the directory path for a given file or directory.
	 * Ensure you append a slash if $path is a directory. This is
	 * because it is not checked if the file/directory exists.
	 * Example: ($path='/var/www/htdocs/', 2) = '/var' while
	 *          ($path='/var/www/htdocs', 2)  = '/'.
	 *
	 * @param string $path
	 * @param integer $numberOfSteps
	 * @return string
	 */
	public function bubbleUpstairs(string $path, int $numberOfSteps): string {
		$rPos = strrpos($path, '/');
		$canTakeOneMoreStep = ($rPos !== 0 && $rPos !== FALSE);

		while ($canTakeOneMoreStep && $numberOfSteps >= 0) {
			$path = (string) substr($path, 0, strrpos($path, '/'));
			$rPos = strrpos($path, '/');
			$canTakeOneMoreStep = ($rPos !== 0 && $rPos !== FALSE);
			$numberOfSteps--;
		}

		return $path;
	}

	/**
	 * Checks of an url defined by the given path points to this site.
	 *
	 * @param string $path
	 * @return boolean
	 */
	public function urlPointsToThisSite(string $path): bool {
		return (str_contains($this->stripProtocol($path), $this->stripProtocol($this->getSiteUrl())));
	}

	/**
	 * Tells whether the given path is absolute or not.
	 * This means that the path starts with '/' or not.
	 *
	 * @param string $path
	 * @return boolean
	 */
	public function isAbsoluteFilesystemPath(string $path): bool {
		return (str_starts_with($path, '/'));
	}

	/**
	 * Returns the URL of the site root.
	 * E.g. 'http://www.mysite.com/'.
	 *
	 * @return string
	 */
	public function getSiteUrl(): string {
		try {
			return GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
		} catch (UnexpectedValueException $exception) {
			return '';
		}
	}

	/**
	 * Tells if the given path contains the HTTP or HTTPS protocol.
	 *
	 * @param string $path
	 * @return boolean
	 */
	public function containsProtocol(string $path): bool {
		/** @noinspection HttpUrlsUsage */
		return (
			str_contains($path, 'http://') ||
			str_contains($path, 'https://')
		);
	}

	/**
	 * Removes 'http://' and 'https://' from the given path.
	 *
	 * @param string $path
	 * @return string
	 */
	protected function stripProtocol(string $path): string {
		/** @noinspection HttpUrlsUsage */
		return str_replace(['https://', 'http://'], '', $path);
	}

	/**
	 * Tells if a file described by the given path exists.
	 *
	 * @param string $path
	 * @param bool $lossy
	 * @return boolean
	 */
	public function fileExists(string $path, bool $lossy = TRUE): bool {
		$exists = file_exists($path);

		// if not exists try again with a normalized path
		if (!$exists && $lossy) {
			$path = $this->getPathToWebRoot() . '/' . $this->normalizePath($path)['normalizedPath'];
			$exists = file_exists($path);
		}

		return $exists;
	}

	/**
	 * Returns the content of the file with the given path.
	 *
	 * @param string $path
	 * @return string
	 */
	public function getFileContent(string $path): string {
		$content = '';
		if ($this->fileExists($path)) {
			$content = file_get_contents(
				$this->getPathToWebRoot() . '/' . $this->normalizePath($path)['normalizedPath']
			);
		}

		return $content;
	}

	/**
	 * Returns the absolute path to the TYPO3 main dir in the filesystem.
	 *
	 * @return string
	 */
	public function getPathToWebRoot(): string {
		return Environment::getPublicPath() . '/';
	}
}
