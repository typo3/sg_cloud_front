<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (http://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgCloudFront\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SGalinski\SgCloudFront\Service\ResourceExtractionService;
use SGalinski\SgCloudFront\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Http\StreamFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class for the ResourceReplacerMiddleware hook
 */
class ResourceReplacerMiddleware implements MiddlewareInterface {
	/**
	 * @var ResourceExtractionService
	 */
	protected $resourceExtractionService;

	/**
	 * @var array
	 */
	protected $configuration = [];

	/**
	 * Constructor
	 */
	public function __construct(ResourceExtractionService $resourceExtractionService) {
		$this->resourceExtractionService = $resourceExtractionService;
		$this->configuration = ExtensionUtility::getExtensionConfiguration();
	}

	/**
	 * @inheritDoc
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$response = $handler->handle($request);
		if (!array_key_exists('cloudFront_enabled', $this->configuration)
			|| !$this->configuration['cloudFront_enabled']
			|| GeneralUtility::_GET('cloudFrontDisabled')
		) {
			return $response;
		}

		$content = $this->replaceResources((string) $response->getBody());

		// StreamFactory is not available in TYPO3 9 yet.
		$streamFactory = GeneralUtility::makeInstance(StreamFactory::class);
		$stream = $streamFactory->createStream($content);

		$response = $response->withBody($stream);
		if ($response->hasHeader('Content-Length')) {
			// Update the Content-Length Header because contrary to the belief that it is calculated after this middleware,
			// the cms-frontend has calculated it in $handler->handle() and it is no longer accurate after processing the
			// scripts
			$response = $response->withHeader('Content-Length', (string) $response->getBody()->getSize());
		}

		return $response;
	}

	/**
	 * old replacement function, extracted
	 *
	 * @param $content
	 * @return array|mixed|string|string[]|null
	 */
	private function replaceResources($content) {
		// shortSiteUrl, Example: google.com
		$shortSiteUrl = GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
		$positionOfSlashes = strpos($shortSiteUrl, '//');
		if ($positionOfSlashes !== FALSE) {
			$shortSiteUrl = substr($shortSiteUrl, $positionOfSlashes + 2);
		}

		$positionOfWww = strpos($shortSiteUrl, 'www.');
		if ($positionOfWww !== FALSE) {
			$shortSiteUrl = substr($shortSiteUrl, $positionOfWww + 4);
		}
		$domainPattern = '^.*?//(www\.)?' . preg_quote($shortSiteUrl, '/');

		$cloudFrontUrl = $this->configuration['cloudFrontUrl'];
		if (!$shortSiteUrl || !$cloudFrontUrl) {
			return $content;
		}
		$cloudFrontDomainPattern = preg_quote($cloudFrontUrl, '/');
		$cloudFrontDomainPattern = str_replace(['https\:', 'http\:'], '(?:https?)\:', $cloudFrontDomainPattern);

		$domainPatternForReplace = '(?:https?\:\/\/)(?:www\.)?' . preg_quote($shortSiteUrl, '/');
		$cloudFrontDomainPatternForReplace = '(?:' . $cloudFrontDomainPattern . ')';
		$domainPatternsForReplace = '(?:' . $domainPatternForReplace . '|' . $cloudFrontDomainPatternForReplace . ')';

		$replacedPaths = [];
		$collectedPatterns = [];
		$collectedReplacements = [];
		$counter = 0;
		$replacementLimit = $this->configuration['replacementLimit'] ?? 9999;
		$allPaths = $this->resourceExtractionService->extractResourcePaths($content);
		foreach ($allPaths as $path) {
			// possible error if the source is too small
			if (strlen($path['original']) <= 5) {
				continue;
			}

			// duplicate check
			if (isset($replacedPaths[$path['original']])) {
				continue;
			}

			if (str_starts_with($path['original'], 'data:')) {
				continue;
			}

			if (str_starts_with($path['original'], '//')) {
				continue;
			}

			if (str_contains($path['normalizedPath'], '.php')) {
				continue;
			}

			$isSameDomain = preg_match('~' . $domainPattern . '~', $path['original']);
			if (!$isSameDomain && preg_match('~^.*://~', $path['original'])) {
				continue;
			}

			if (!$isSameDomain) {
				if (str_starts_with($path['original'], '/')) {
					$path['original'] = substr($path['original'], 1);
				}

				if (str_starts_with($path['original'], '\'') || str_starts_with($path['original'], '"')) {
					$path['original'] = trim($path['original'], '\'"');
				}

				$cloudFrontResource = $cloudFrontUrl . $path['original'];
			} else {
				$cloudFrontResource = preg_replace('~/?' . $domainPattern . '~', $cloudFrontUrl, $path['original']);
				if ($cloudFrontResource === $path['original']) {
					continue;
				}
			}

			if (str_starts_with($path['original'], '/') || preg_match('~' . $domainPattern . '~', $path['original'])) {
				$pathWithStartingSlash = preg_quote($path['original'], '/');
				$pathWithoutStartingSlash = preg_quote(substr($path['original'], 1), '/');
			} else {
				$pathWithStartingSlash = preg_quote('/' . $path['original'], '/');
				$pathWithoutStartingSlash = preg_quote($path['original'], '/');
			}

			$paths = $pathWithoutStartingSlash . '|' . $pathWithStartingSlash;
			$pattern = '/(["\'(,]\s*)(' . $domainPatternsForReplace . $paths . ')(\s*[0-9"\')])/';

			$collectedPatterns[] = $pattern;
			$collectedReplacements[] = '$1' . $cloudFrontResource . '$3';

			$replacedPaths[$path['original']] = TRUE;

			++$counter;
			if ($counter > $replacementLimit) {
				break;
			}
		}

		if (count($collectedPatterns) > 0) {
			$content = preg_replace($collectedPatterns, $collectedReplacements, $content);
		}

		return $content;
	}
}
