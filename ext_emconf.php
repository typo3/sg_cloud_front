<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'sgalinski Amazon CloudFront',
	'description' => 'Implements the Amazon CloudFront Service for TYPO3',
	'category' => 'plugin',
	'version' => '4.0.0',
	'state' => 'stable',
	'author' => 'Fabian Galinski',
	'author_email' => 'fabian@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' =>
		[
			'depends' =>
				[
					'typo3' => '12.4.00-12.4.99',
					'php' => '8.1.99-8.3.99',
				],
			'conflicts' =>
				[
				],
			'suggests' =>
				[
					'scriptmerger' => '',
				],
		],
];
