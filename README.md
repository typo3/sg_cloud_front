# sg_cloud_front

Integration of the Amazon Web Services - CloudFront CDN

# Configuration Instructions

## Creation of a new CloudFront Distribution

1. Login into the AWS console (https://console.aws.amazon.com)
2. Click on "Services" and select "CloudFront" within "Networking & Content Delivery"
3. Create a new distribution. AWS will show you the way to go at this point.

Note: It's recommended that you add an Alternate Domain Name (CNAME) like "cdn.sgalinski.de"

## Enable CloudFront for the TYPO3 site

1. Make sure, that this extension is enabled in the extension manager.
2. Open the configuration view within the extension manager for this extension.
3. Enable the CDN and add your CNAME with the host and a tailing slash in "Cloud Front URL which replaces the
   resources." The value should look like "https://cdn.yourdomain.de/"

# Debugging hints

You can disable cloudFront for one page.
Example: https://www.yourdomain.de/news/?cloudFrontDisabled=1
